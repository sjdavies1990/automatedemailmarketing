from pymongo import MongoClient
import sys


class AddEmailTemplate:

    """
        Shallow class to for Add Email Template jenkins job
    """

    def __init__(self, email_template_id, text, personalisation_list=None):
        self.col = MongoClient('localhost', 27017).resources.email_templates
        if not self.col.find_one({'email_template_id': email_template_id}):
            email_template_doc = {
                'email_template_id': email_template_id,
                'text': text,
                'personalisation_list': personalisation_list,
                'meta_dict': {
                    'read': 0,
                    'responses': 0,
                    'landing_page_click_through': 0
                }
            }
            self.col.insert_one(email_template_doc)
        else:
            print('email_template_id (%s) already exists. Exiting...' % email_template_id)
            Exception()


if __name__ == '__main__':
    AddEmailTemplate(
        email_template_id=sys.argv[1],
        text=sys.argv[2],
        personalisation_list=sys.argv[3].split(',') if len(sys.argv) == 4 else None
    )