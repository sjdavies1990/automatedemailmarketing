from pymongo import MongoClient
import sys


class AddSendAddress:

    """
        Shallow class to Add Send Address jenkins job
    """

    def __init__(self, send_address, signature):
        self.col = MongoClient('localhost', 27017).resources.send_address
        if not self.col.find_one({'send_address': send_address}):
            sub_domain_list = send_address.split('@')[0].split('.')
            address_doc = {
                'send_address': send_address,
                'signature': signature,
                'domain': send_address.split('@')[1],
                'subdomain': sub_domain_list[1] if len(sub_domain_list) != 1 else None,
                'meta_dict': {
                    'sent': 0,
                    'read': 0,
                }
            }
            self.col.insert_one(address_doc)
        else:
            print('send_address (%s) already exists. Exiting' % send_address)
            Exception()


if __name__ == '__main__':
    AddSendAddress(
        send_address=sys.argv[1],
        signature=sys.argv[2]
    )
