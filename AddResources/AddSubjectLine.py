from pymongo import MongoClient
import sys


class AddSubjectLine:

    """
        Shallow class to Add Subject Line jenkins job
    """

    def __init__(self, subject_line_id, text, personalisation_list=''):
        self.col = MongoClient('localhost', 27017).resources.subject_lines
        if not self.col.find_one({'subject_line_id': subject_line_id}):
            subject_line_doc = {
                'subject_line_id': subject_line_id,
                'text': text,
                'personalisation_list': personalisation_list.split(','),
                'meta_dict': {
                    'sent': 0,
                    'read': 0
                }
            }
            self.col.insert_one(subject_line_doc)
        else:
            print('subject_line_id (%s) already exists. Exiting' % subject_line_id)
            Exception()


if __name__ == '__main__':
    AddSubjectLine(
        subject_line_id=sys.argv[1],
        text=sys.argv[2],
        personalisation_list=sys.argv[3] if len(sys.argv) == 4 else ''
    )

