from pymongo import MongoClient
from datetime import datetime
import time
import sys


class CreateCampaign:
    """
        Shallow class used to create email send campaigns from subsequent Jenkins job

        * dates in format dd-mm-yyyy

    """

    def __init__(
            self,
            campaign_id,
            date_start,
            date_end,
            send_address_list,
            sector_target_list,
            email_template_list,
            subject_line_list,
            daily_vol,
            time_start=8,
            time_end=16,
            warmup_period=14,
            warmup_initial_vol=0.2,
            warmup_name_ratio=0.75
    ):
        self.col = MongoClient('localhost', 27017).resources.campaign_configs
        if self.col.find_one({'campaign_id': campaign_id}):
            print('Campaign_ID already exists, exiting...')
            Exception()
        else:
            campaign_config = {
                'campaign_id': campaign_id,
                'date_start': time.mktime(datetime.strptime(date_start, "%d-%m-%Y").timetuple()),
                'date_end': time.mktime(datetime.strptime(date_end, "%d-%m-%Y").timetuple()),
                'send_address_list': send_address_list,
                'sector_target_list': [val.replace('_', ' ') for val in sector_target_list],
                'email_template_list': email_template_list,
                'subject_line_list': subject_line_list,
                'daily_vol': int(daily_vol),
                'time_dict': {
                    'start': float(time_start),
                    'end': float(time_end)
                },
                'warmup_dict': {
                    'period': int(warmup_period),
                    'initial_vol': float(warmup_initial_vol),
                    'name_ratio': float(warmup_name_ratio)
                },
                'meta_dict': {
                    'sent': 0,
                    'reads': 0,
                    'responses': 0,
                    'click_through': 0,
                    'bounces': 0
                }
            }
            self.col.insert_one(campaign_config)


if __name__ == '__main__':
    CreateCampaign(
        campaign_id=sys.argv[1],
        date_start=sys.argv[2],
        date_end=sys.argv[3],
        send_address_list=sys.argv[4].split(','),
        sector_target_list=sys.argv[5].split(','),
        email_template_list=sys.argv[6].split(','),
        subject_line_list=sys.argv[7].split(','),
        daily_vol=sys.argv[8],
        time_start=sys.argv[9] if len(sys.argv) > 9 else 8,
        time_end=sys.argv[10] if len(sys.argv) > 10 else 16,
        warmup_period=sys.argv[11] if len(sys.argv) > 11 else 14,
        warmup_initial_vol=sys.argv[12] if len(sys.argv) > 12 else 0.2,
        warmup_name_ratio=sys.argv[13] if len(sys.argv) > 13 else 0.75
    )