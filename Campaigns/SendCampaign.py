from pymongo import MongoClient
from numpy import random
import logging
import requests
import Settings
import time
import sys
import os


class SendCampaign:

    """
        *TODO can multithread by email_server
    """

    def __init__(self, campaign_id, min_ts_cooldown=2592000):
        cl = MongoClient('localhost', 27017)
        self.min_ts_cooldown = min_ts_cooldown
        self.base_coll = cl.base.email_data
        self.resource_db = cl.resources
        self.campaign_config = cl.resources.campaign_configs.find_one({'campaign_id': campaign_id})
        self.subject_line_dict = {}
        for subject_line_id in self.campaign_config['subject_line_list']:
            self.subject_line_dict[subject_line_id] = cl.resources.subject_lines.find_one(
                {
                    'subject_line_id': subject_line_id
                }
            )
        logging.debug('Subject line list: %s' % self.subject_line_dict)
        self.email_template_dict = {}
        for email_template_id in self.campaign_config['email_template_list']:
            self.email_template_dict[email_template_id] = cl.resources.email_templates.find_one(
                {
                    'email_template_id': email_template_id
                }
            )
        logging.debug('Email template dict: %s' % self.email_template_dict)
        self.send_list_dict = {}
        for address in self.campaign_config['send_address_list']:
            self.send_list_dict[address] = cl.resources.send_address.find_one(
                {
                    'send_address': address
                }
            )
        logging.debug('Send list dict: %s' % self.send_list_dict)

    def pull_resource_dict(self, domain, file_list):
        resource_dict = {}
        folder_path = '%s/resources/lib/%s' % (Settings.AEM_PATH, domain)
        file_list = file_list if file_list != ['ALL'] else \
            os.listdir(folder_path)
        for file_ in file_list:
            with open('%s/%s' % (folder_path, file_), 'r') as resource_file:
                resource_dict[file_] = resource_file.read()

    def random_choice(self, choice_list):
        if type(choice_list) == dict:
            return choice_list[list(choice_list.keys())[random.randint(len(choice_list.keys()))]]
        else:
            return choice_list[random.randint(len(choice_list))]

    def get_email(self, is_warmup_period, email_server):
        """
            Returns a dict with all the details for the email
        """
        latest_viable_ts = time.time() - self.min_ts_cooldown
        send_dict = dict()
        send_dict['business'] = self.base_coll.find_one({
            "$and": [
                {
                    'primary_class': {
                        '$in': self.campaign_config['sector_target_list']
                    }
                } if self.campaign_config['sector_target_list'] != ['ALL'] else {},
                {
                    'last_campaign_ts': {
                        '$lt': latest_viable_ts
                    }
                },
                {
                    'email_server': {
                        '$eq': email_server
                    }
                } if email_server else {},
                {
                    'sender_name': {
                        '$ne': ''
                    }
                } if is_warmup_period and random.rand() < self.campaign_config['warmup_dict']['name_ratio'] else {},
            ]
        })
        send_dict['subject_line'] = self.random_choice(self.subject_line_dict)
        send_dict['email_template'] = self.random_choice(self.email_template_dict)
        send_dict['from_address'] = self.random_choice(self.send_list_dict)
        send_dict['comp_email'] = '%s \n%s' % (
                self.get_comp_text(send_dict['email_template'], send_dict['business']),
                eval("f'%s'" % send_dict['from_address']['signature'])
        )
        logging.debug('Send dict: %s' % send_dict)
        return send_dict

    def get_comp_text(self, text_dict, business_detail_dict):
        """
            Creating as using for subject_line and email_template
        """
        name = business_detail_dict['sender_name'] if business_detail_dict['sender_name'] else 'there'
        business_name = business_detail_dict['name']
        sector = business_detail_dict['primary_class']
        website = business_detail_dict['website']
        return eval("f'%s'" % text_dict['text'])

    def send_email(self, is_warmup_period, email_server=None):
        send_dict = self.get_email(
            email_server=email_server,
            is_warmup_period=is_warmup_period
        )
        req = requests.post(
            'https://api.eu.mailgun.net/v3/%s/messages' % send_dict['from_address']['domain'],
            auth=('api', Settings.API_DICT[send_dict['from_address']['domain']]),
            data={
                'from': send_dict['from_address']['send_address'],
                'to': [send_dict['business']['primary_email']],
                'subject': self.get_comp_text(send_dict['subject_line'], send_dict['business']),
                'text': send_dict['comp_email'],
                'o:tracking': 'yes',
                'o:tracking-opens': 'yes'
            }
        )
        self.update_records(send_dict)
        if req.status_code == 250 or req.status_code == 200:
            logging.info('Email sent to: %s' % send_dict['business']['primary_email'])
        else:
            logging.warning('Issue sending to email: %s\nStatus code: %s' % (
                send_dict['business']['primary_email'],
                req.status_code
            ))

    def update_records(self, send_dict):
        """
            Need to update records for:
                -   business
                -   subject_line
                -   from_address
                
        """

        # Business doc update
        business_doc = send_dict['business']
        business_doc['meta_dict']['campaign_log'][self.campaign_config['campaign_id']] = {
            'timestamp_sent': int(time.time()),
            'is_response': 0,
            'is_read': 0,
            'sent_from': send_dict['from_address']['send_address'],
            'email_template_id': send_dict['email_template']['email_template_id'],
            'subject_line_id': send_dict['subject_line']['subject_line_id']
        }
        business_doc['last_campaign_ts'] = int(time.time())
        business_doc['num_campaigns'] += 1
        self.base_coll.find_one_and_replace(
            {
                '_id': business_doc.pop('_id'),
            },
            business_doc
        )
        logging.debug('Campaign log updated')

        # TODO when adding reporting etc
        # # Subject line update
        # self.subject_line_dict = send_dict['subject_line']
        # print(subject_line_dict.keys())
        # subject_line_dict['meta_dict']['sent'] += 1
        # self.resource_db['subject_lines'].find_one_and_replace(
        #     {
        #         '_id': subject_line_dict.pop('_id')
        #     },
        #     subject_line_dict
        # )
        # logging.debug('Subject line metadata updated')
        #
        # # From address update
        # from_address_dict = send_dict['from_address']
        # from_address_dict['meta_dict']['sent'] += 1
        # self.resource_db['send_address'].find_one_and_replace(
        #     {
        #         '_id': from_address_dict.pop('_id')
        #     },
        #     from_address_dict
        # )
        # logging.debug('From address metadata updated')
        # TODO: campaign config metadata update

    def daily_send_campaign(self):
        logging.info('Starting campaign: %s' % self.campaign_config['campaign_id'])
        is_warmup_period = time.time() - self.campaign_config['date_start'] < self.campaign_config['warmup_dict']['period']*24*60*60
        logging.info('Is warmup period: %s' % is_warmup_period)
        seconds_in_day = int(self.campaign_config['time_dict']['end'] - self.campaign_config['time_dict']['start'])*60*60
        daily_volume = int(
            self.campaign_config['daily_vol'] * (self.campaign_config['warmup_dict']['initial_vol']
            if is_warmup_period else 1)
        )
        logging.info('Planning to send out %s email today' % daily_volume)
        sleep_period = int(seconds_in_day/daily_volume)
        logging.info('Average period between sends: %s' % sleep_period)
        for _ in range(daily_volume):
            self.send_email(is_warmup_period=is_warmup_period)
            sleep_interval = 2*sleep_period
            time.sleep(random.randint(sleep_interval))


if __name__ == '__main__':
    logging.basicConfig(level=eval('logging.%s' % sys.argv[2] if len(sys.argv) == 3 else 'INFO'))
    SendCampaign(sys.argv[1]).daily_send_campaign()