from pymongo import MongoClient
from datetime import datetime
import pandas as pd
import Settings
import time


class PullColdCall:

    def __init__(self, campaign_id, keyword_list, email_list):
        self.campaign_id = campaign_id
        self.keyword_list = keyword_list
        self.email_list = email_list
        self.cl = MongoClient('localhost', 27017)

    def update_records(self, doc_list, col):
        for doc in doc_list:
            doc['campaign_log'][self.campaign_id] = {
                'timestamp_sent': int(time.time()),
                'type': 'cold call'
            }
            doc['last_campaign_ts'] = int(time.time())
            doc['num_campaigns'] += 1
            col.find_one_and_replace(
                {
                    '_id': doc.pop('_id')
                },
                doc
            )

    def pull_call_data(self):
        raw_df = pd.DataFrame(list(self.cl.raw.phone_data.find(
            {
             'keywords': {
                    '$in': self.keyword_list
                }
            },
            {
                'name': 1,
                'phone': 1,
                'address': 1,
                'keywords': 1,
            }
        )))
        return raw_df
        #raw_df['primary_email'] = ''
        #raw_df['website'] = ''
        # base_df = pd.DataFrame(list(self.cl.base.email_data.find(
        #     {
        #      'keywords': {
        #             '$in': keyword_list
        #         }
        #     },
        #     {
        #         'name': 1,
        #         'phone': 1,
        #         'address': 1,
        #         'keywords': 1,
        #         'primary_email': 1,
        #         'website': 1,
        #     }
        # )))
        #return pd.concat([raw_df, base_df])

    def package_and_send(self):
        raw_data = self.pull_call_data()
        print(len(raw_data))
        raw_data.to_csv(
            '%s/tmp/tmp_data/cold_call_data_%s.csv' % (Settings.AEM_PATH, datetime.today().strftime('%Y-%m-%d')),
            index=False,
        )


PullColdCall(campaign_id='', keyword_list=['debt collectors'], email_list=[]).package_and_send()