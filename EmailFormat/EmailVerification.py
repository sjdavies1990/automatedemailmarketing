from pymongo import MongoClient
import pandas as pd
import Settings
import logging
import sys
import os


class EmailVerification:

    def __init__(
            self, verification_location='resources/verification',
            non_business_domains=['gmail', 'hotmail', 'yahoo', 'aol', 'donate']
                ):
        self.location = '%s/%s' % (Settings.AEM_PATH, verification_location)
        self.cl = MongoClient('localhost', 27017)
        self.non_business_domains = non_business_domains
        self.coll_dict = {
            'input': self.cl.base.unverified,
            'tmp': self.cl.base.tmp,
            'output': self.cl.base.email_data
        }

    def input_2_tmp(self):
        doc_list = list(self.coll_dict['input'].find())
        with open('%s/output.txt' % self.location, 'w') as f:
            for doc in doc_list:
                self.coll_dict['input'].delete_one({'_id': doc.pop('_id')})
                self.coll_dict['tmp'].insert_one(doc)
                for email_address in doc['email_list']:
                    f.write('%s\n' % email_address)

    def tmp_2_output(self):
        email_df = pd.read_csv('%s/Results.csv' % self.location, sep='delimiter')
        email_df.columns = ['email-address'] if len(email_df.iloc[0]) == 1 else ['email-address', '']
        valid_filtered = [
            address.strip(',').strip('"') for address in list(email_df['email-address'])
            if not any(domain in address for domain in self.non_business_domains)
        ]
        for doc in list(self.coll_dict['tmp'].find()):
            if doc['primary_email'] in valid_filtered:
                doc.pop('_id')
                self.coll_dict['output'].insert_one(doc)
            elif len(doc['email_list']) != 1:
                i = 0
                while i < len(doc['email_list']):
                    if doc['email_list'][i] in valid_filtered:
                        doc['primary_email'] = doc['email_list'][i]
                        doc.pop('_id')
                        self.coll_dict['output'].insert_one(doc)
                    i += 1
                    if i < len(doc['email_list']) and 'phone' in doc.keys():
                        doc.pop('_id')
                        doc.pop('primary_email')
                        doc.pop('email_list')
                        self.cl.raw.phone_data.insert_one(doc)
                        break
        self.cl.base.drop_collection('tmp')
        os.remove('%s/output.txt' % self.location)
        os.remove('%s/Results.csv' % self.location)


if __name__ == '__main__':
    logging.basicConfig(level=eval('logging.%s' % sys.argv[1]))
    if sys.argv[2] == 'input':
        EmailVerification().input_2_tmp()
    else:
        EmailVerification().tmp_2_output()
