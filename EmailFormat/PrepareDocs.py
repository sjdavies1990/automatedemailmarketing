from pymongo import MongoClient, TEXT
from difflib import SequenceMatcher
import Settings
import dns.resolver
import numpy as np
import logging
import pickle
import sys
import re


class PrepareDocs:

    """
        Takes raw business data and produces a unified base data format for which to run campaigns from
        *TODO improve regex
        *TODO name_list to db
        doc['campaign_log'] = {
            campaign_id: {
                'timestamp_sent':
                'is_response':
                'is_read':
                'sent_from':
                'email_template_id':
                'subject_line_id':
            }
            , {...}, ...
        }
        *TODO many no such domains, check if actually working...
    """

    def __init__(self, name_list_location='%s/resources/lib/name_list' % Settings.AEM_PATH):
        self.cl = MongoClient('localhost', 27017)
        self.raw_coll = self.cl.raw.email_data
        self.base_coll = self.cl.base.unverified
        with open(name_list_location, 'rb') as name_file:
            self.name_list = pickle.load(name_file)

    def get_primary_email(self, name, email_list):
        email_list = [
            email for email in email_list if
            re.match(
                '^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$',
                email
            )
        ]
        if len(email_list) != 0:
            domain_similarity_list = [
                SequenceMatcher(None, name, email.split('@')[1].split('.')[0]).ratio()
                for email in email_list
            ]
            remaining_emails = []
            for i in range(len(domain_similarity_list)):
                if domain_similarity_list[i] > max(domain_similarity_list) - 0.05:
                    remaining_emails.append(email_list[i])
            email_name_list = []
            for email in remaining_emails:
                try:
                    email_prefix = email.split('@')[0].split('.')[0]
                    name_ = email_prefix if email_prefix in self.name_list else ''
                except:
                    name_ = ''
                email_name_list.append((name_, email))
            for name, email in email_name_list:
                if name != '':
                    return [name, email]
            return ['', email_name_list[0][1]]
        else:
            return ['', None]

    def get_email_server(
            self,
            email,
            common_servers=('google', 'outlook', 'yahoo', '1and1', 'messagelabs', 'other')
    ):
        try:
            mx_list = str(dns.resolver.query(email.split('@')[1], 'MX')[0]).lower()
            for server in common_servers[:-1]:
                if server in mx_list:
                    return server
            return common_servers[-1]
        except:
            logging.info('No such domain for email: %s' % email)
            return np.NaN

    def run(self):
        for doc in list(self.raw_coll.find()):
            email_details = self.get_primary_email(doc['name'], doc['email_list'])
            smtp_server = self.get_email_server(email_details[1]) if email_details[1] else None
            if email_details[1] and smtp_server:
                doc['sender_name'] = email_details[0]
                doc['primary_email'] = email_details[1]
                doc['email_server'] = self.get_email_server(doc['primary_email'])
                doc['meta_dict']['campaign_log'] = {}
                doc['meta_dict']['smtp_server'] = smtp_server
                doc['last_campaign_ts'] = 0
                doc['num_campaigns'] = 0
                self.base_coll.insert_one(doc)
            self.raw_coll.delete_one({'_id': doc['_id']})
        self.base_coll.create_index([('primary_class', TEXT)], name='search_index', default_language='english')


if __name__ == '__main__':
    logging.basicConfig(level=eval('logging.%s' % sys.argv[1]))
    PrepareDocs().run()
