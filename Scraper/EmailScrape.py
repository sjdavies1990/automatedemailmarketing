import multiprocessing.dummy as mp
from bs4 import BeautifulSoup
from pymongo import MongoClient
import queue
import threading
import requests
import logging
import sys
import re


class EmailScrape:

    """
        *TODO get_emails can be a LOT better
    """

    def __init__(self, max_threads=25):
        self.max_threads = max_threads
        self.raw_db = MongoClient('localhost', 27017).raw
        self.coll_dict = {
            'site': self.raw_db.site_data,
            'email': self.raw_db.email_data,
            'phone': self.raw_db.phone_data
        }

    def get_url_list(self, url):
        try:
            page = BeautifulSoup(requests.get(url, timeout=6).content, "html.parser")
        except:
            return []
        all_links_soup = page.find_all("a")
        full_url_list = [
            val["href"]
            for val in all_links_soup
            if val.has_attr("href")
               and url in val["href"]
               and ".png" not in val["href"].lower()
               and ".jpeg" not in val["href"].lower()
               and ".jpg" not in val["href"].lower()
        ]
        logging.debug('full_url_list length: %s' % len(full_url_list))
        url_ = url if url[-1] != "/" else url[:-1]
        sub_url_list = [
            url_ + val["href"]
            for val in all_links_soup
            if val.has_attr("href") and len(val["href"]) > 1 and val["href"][0] == "/"
        ]
        return list(set(full_url_list + [url] + sub_url_list))

    def get_emails(self, url, email_list):
        try:
            r = requests.get(url, timeout=2)
            status_code = r.status_code
        except:
            status_code = -1
        if status_code == 200:
            page_raw = str(r.content)
            email_strings = [
                re.search(r"([\w.])+@([\w.])+", page_raw[i - 40 : i + 40]).group(0)
                for i in [m.start() for m in re.finditer("@", page_raw)]
                if re.search(r"([\w.])+@([\w.])+", page_raw[i - 40 : i + 40])
            ]
            email_list += list(
                set(
                    [
                        val.lower()
                        for val in email_strings
                        if ".png" not in val.lower()
                        and ".jpeg" not in val.lower()
                        and ".jpg" not in val.lower()
                        and ".svg" not in val.lower()
                        and "." in val
                    ]
                )
            )

    def website_email_scrape(self, base_url):
        manager = mp.Manager()
        email_list = manager.list()
        url_list = self.get_url_list(base_url)[:25]
        logging.debug('url list: %s' % url_list)
        logging.info('Length of url list: %s' % len(url_list))
        jobs = []
        for url in url_list:
            u = mp.DummyProcess(target=self.get_emails, args=(url, email_list))
            jobs.append(u)
            u.start()
        for thread in jobs:
            thread.join()
        return list(set(email_list))[:25] if len(email_list) != 0 else []

    def worker_function(self, funct, q):
        while True:
            doc = q.get()
            if doc is None:
                break
            funct(doc)
            q.task_done()

    def single_run(self, doc):
        logging.warning('Starting doc: %s' % doc['name'])
        self.coll_dict['site'].remove({"_id": doc.pop('_id')})
        email_list = self.website_email_scrape(doc['website'])
        logging.info('%s email list size: %s' % (doc['name'], len(email_list)))
        if len(email_list) != 0:
            doc['email_list'] = email_list
            self.coll_dict['email'].insert_one(doc)
        else:
            logging.info('Unable to scrape email from website for %s' % doc['name'])
            if 'phone' in doc.keys():
                logging.info('Moving doc to raw_phone coll')
                self.coll_dict['phone'].insert_one(doc)
            else:
                logging.info('No email or phone, removing doc')
        logging.warning('Finished doc: %s' % doc['name'])

    def scrape_collection(self):
        q = queue.Queue()
        thread_list = []

        logging.info('%s records to loop through' % self.coll_dict['site'].count())
        for doc in list(self.coll_dict['site'].find()):
            q.put_nowait(doc)

        for _ in range(self.max_threads):
            t = threading.Thread(
                target=self.worker_function,
                args=(self.single_run, q)
            )
            t.start()
            thread_list.append(t)

        q.join()
        for i in range(self.max_threads):
            q.put(None)
        for t in thread_list:
            t.join()


if __name__ == '__main__':
    logging.basicConfig(level=eval('logging.%s' % sys.argv[1]))
    EmailScrape(max_threads=int(sys.argv[2])).scrape_collection()
