import multiprocessing.dummy as mp
from bs4 import BeautifulSoup
from selenium import webdriver
import numpy as np
import requests
import logging


class GetProxies:
    def __init__(self, max_failures=3):
        self.proxy_list = []
        self.max_failures = max_failures

    def check_proxy(self, proxy, working_proxy_list):
        try:
            r = requests.get(
                "https://www.yell.com/",
                proxies={"http": proxy, "https": proxy},
                timeout=5,
            )
            logging.debug('proxy: %s, status_code: %s'% (proxy, r.status_code))
            working_proxy_list.append(proxy)
        except:
            pass

    def proxy_list_check(self, proxy_list):
        # Pings google to check if the proxies work
        manager = mp.Manager()
        working_proxy_list = manager.list()
        jobs = []
        for proxy in proxy_list:
            p = mp.DummyProcess(
                target=self.check_proxy, args=(proxy, working_proxy_list)
            )
            jobs.append(p)
            p.start()
        for thread in jobs:
            thread.join()
        return [{'proxy': proxy, 'failures': 0} for proxy in working_proxy_list]

    def proxy_scrape(self):
        # Create database
        try:
            try:
                proxy_soup = BeautifulSoup(
                    requests.get(
                        "https://free-proxy-list.net/anonymous-proxy.html"
                    ).content,
                    "html.parser",
                )
            except:
                proxy_soup = BeautifulSoup(
                    requests.get("https://free-proxy-list.net").content, "html.parser"
                )
            proxy_list = []
            for proxy in (
                proxy_soup.find("table", id="proxylisttable")
                .find("tbody")
                .find_all("tr")
            ):
                proxy_vals = proxy.find_all("td")
                if proxy_vals[4].text in [
                    "anonymous",
                    "elite proxy",
                ]:  # and proxy_vals[3].text in self.eu_countries and:
                    proxy_list.append(
                        {
                            "ip_address": proxy_vals[0].text,
                            "port": proxy_vals[1].text,
                            "proxy": "%s:%s" % (proxy_vals[0].text, proxy_vals[1].text),
                            "country": proxy_vals[3].text,
                            "type": proxy_vals[4].text,
                        }
                    )
            # created proxy_list if needed for future
            self.proxy_list = self.proxy_list_check(
                [val["proxy"] for val in proxy_list]
            )
        except AttributeError as e:
            logging.error(e)
            logging.warning("Proxy site unavailable atm")
            self.proxy_list = []

    def get_proxy_options(self, url, num_failout=5):
        n = 0
        while n < num_failout:
            proxy_ind = np.random.randint(len(self.proxy_list))
            proxy = self.proxy_list[proxy_ind]['proxy']
            try:
                r = requests.get(url, proxies={"http": proxy, "https": proxy})
                status_code = r.status_code
            except:
                status_code = 403
            if status_code != 403:
                return proxy
            else:
                if self.proxy_list[proxy_ind]['failures'] > self.max_failures:
                    self.proxy_list.remove(proxy_ind)
                else:
                    self.proxy_list[proxy_ind]['failures'] += 1
        return -1, ''