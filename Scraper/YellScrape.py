from Scraper.GetProxies import GetProxies
from pymongo import MongoClient
from selenium import webdriver
from bs4 import BeautifulSoup
from pyvirtualdisplay import Display
import Settings
import numpy as np
import logging
import time
import sys


class YellScrape:

    """
        *TODO: multithread the next locations
        *TODO: improve scrape coverage
        *TODO: add raw_website_list & raw_call_list to be pulled from db
        *TODO: setting file for paths
    """

    def __init__(self, keywords, locations=("UK",), max_loc_depth=3, run_proxy=True, base_sleep_time=30):
        self.run_proxy = run_proxy
        self.base_sleep_time = base_sleep_time
        self.base_site = 'https://www.yell.com/ucs/UcsSearchAction.do'
        self.keywords = keywords
        self.cl = MongoClient('localhost', 27017)
        self.raw_db = self.cl['raw']
        self.scrape_db = self.cl['scrape_data']
        self.past_locations = []
        self.duplication_dict = {
            'phone': {},
            'website': {}
        }
        self.next_locations = locations
        if self.run_proxy:
            self.GP = GetProxies()
        self.max_loc_depth = max_loc_depth
        self.scrape_name_failures = 0

    def get_page(self, url, driver):
        # Need to define driver first
        driver.get(url)
        driver.implicitly_wait(2)
        page = BeautifulSoup(driver.page_source, "html.parser")
        return page

    def get_business_details(self, article, location):
        viability = {
            val.text.replace(' ', '').replace("\n", ""): val
            for val in
            article.find_all("div", class_="businessCapsule--ctaItem") +
            article.find_all("a", class_="businessCapsule--ctaItem")
        }
        if 'Website' in viability.keys() or 'Call' in viability.keys():
            try:
                business_details = {
                    'name': article.find('a', class_='businessCapsule--title').text.replace('\n', ''),
                    'keywords': keywords,
                    'meta_dict': {
                        'data_source': 'Yell',
                        'location': location,
                    },
                }
                logging.info('Business Name: %s' % business_details['name'])
                if 'Website' in viability.keys():
                    business_details['website'] = viability['Website']['href'].split("?utm_source")[0]
                if 'Call' in viability.keys():
                    business_details['phone'] = article.find(
                        "span", class_="business--telephoneNumber"
                    ).text.replace(" ", "")
                try:
                    business_details['class_list'] = [
                            val.text.replace("\n", "")
                            for val in (
                                article.find_all(
                                    "span", class_="businessCapsule--classification"
                                )
                                + article.find_all(
                                    "a", class_="businessCapsule--classification"
                                )
                            )
                        ]
                    business_details['primary_class'] = business_details['class_list'][0].lower()
                except:
                    logging.info('Name: %s, classification failed' % business_details['name'])
                    business_details['class_list'] = ['no_classification']
                    business_details['primary_class'] = None
                try:
                    business_details['address'] = article.find(
                        "a", class_="businessCapsule--address"
                    ).text.replace("\n", "")
                except:
                    logging.info('Name: %s, address failed' % business_details['name'])
                primary_class = business_details['primary_class']
                if 'website' in business_details.keys():
                    self._check_duplicates_and_insert(
                        business_details,
                        primary_class,
                        self.raw_db.site_data,
                        'website'
                    )
                elif 'phone' in business_details.keys():
                    self._check_duplicates_and_insert(
                        business_details,
                        primary_class,
                        self.raw_db.phone_data,
                        'phone'
                    )
            except IOError:
                self.scrape_name_failures += 1
                logging.warning('Scrape name failure: %s' % self.scrape_name_failures)
        else:
            logging.info('Unable to scrape website from article')

    def _check_duplicates_and_insert(self, business_details, primary_class, coll, doc_type='website'):
        # Using same functionality over website and phone, hence abstracting
        if primary_class not in self.duplication_dict[doc_type].keys():
            db_scrape_results = self.scrape_db[doc_type].find_one(
                {'primary_class': primary_class}, {'_id': 0, 'data': 1}
            )
            self.duplication_dict[doc_type][primary_class] = db_scrape_results['data'] if db_scrape_results else []
        if business_details[doc_type] not in self.duplication_dict[doc_type][primary_class]:
            coll.insert_one(business_details)
            self.duplication_dict[doc_type][primary_class].append(business_details[doc_type])
            if len(self.duplication_dict[doc_type][primary_class]) % 10 == 0:
                self._sub_update_duplication_data(doc_type, primary_class)

    def _sub_update_duplication_data(self, doc_type, primary_class):
        self.scrape_db[doc_type].update(
            {
                'primary_class': primary_class
            },
            {
                '$set': {
                    'primary_class': primary_class,
                    'data': self.duplication_dict[doc_type][primary_class]
                }
            },
            upsert=True
        )

    def _comp_update_duplication_data(self):
        for doc_type in self.duplication_dict.keys():
            for primary_class in self.duplication_dict[doc_type].keys():
                self._sub_update_duplication_data(doc_type, primary_class)

    def is_last_page(self, page):
        page_ = page.find("div", id="rightNav")
        try:
            return page_.find("span", class_="btn-disabled").text == "Next"
        except:
            logging.warning("Last page: %s" % page)
            return True

    def is_increase_scope(self, souped_page):
        if len([val.text for val in souped_page.find_all("a", class_="btn-grey")]) == 9:
            self.next_locations += [
                val.text.replace("\n", "").split(",")[0]
                for val in souped_page.find("div", id="filters")
                .find_all("div", class_="filters--section")[2]
                .find_all("a")
            ]

    def scrape_location(self, keywords, location, rerun=False):
        logging.info("Starting location: %s" % location)
        self.past_locations.append(location)
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('window-size=800x841')
        if self.run_proxy:
            proxy = self.GP.get_proxy_options(url=(
                    "%s?location=%s&keywords=%s"
                    % (self.base_site, location, keywords.replace(" ", "+"))
                ))
            chrome_options.add_argument("--proxy-server=%s" % proxy)
        if not self.run_proxy or proxy != -1:
            if sys.platform == 'linux':
                display = Display(visible=0, size=(800, 800))
                display.start()
            driver = webdriver.Chrome(
                Settings.DRIVER_PATH,
                options=chrome_options
            )
            last_page = False
            page_num = 1
            while not last_page:
                logging.info('Page Number: %s' % page_num)
                url = (
                    "%s?pageNum=%s&location=%s&keywords=%s"
                    % (self.base_site, page_num, location, keywords.replace(" ", "+"))
                )
                page = self.get_page(url, driver)
                caps_list_sect = page.find("div", class_="results--capsuleList")
                if caps_list_sect:
                    for article in caps_list_sect.find_all(
                        "article", class_="businessCapsule"
                    ):
                        self.get_business_details(article, location)
                    if page_num == 1:
                        self.is_increase_scope(page)
                    page_num += 1
                else:
                    last_page = True
                time.sleep(2 * self.base_sleep_time * np.random.rand())
            logging.info('Exiting driver')
            driver.quit()
            if page_num == 1 and not rerun:
                # Rerunning over new proxy
                self.scrape_location(keywords, location, rerun=True)
            logging.info("%s finished with %s pages" % (location, page_num))
        else:
            logging.error('Unable to scrape location: %s' % location)

    def scrape_yell(self):
        logging.info("Scraping proxies")
        if self.run_proxy:
            self.GP.proxy_scrape()
            logging.info("%s Proxies scraped" % len(self.GP.proxy_list))
        loc_depth = 0
        try:
            while loc_depth < self.max_loc_depth and len(self.next_locations) != 0:
                next_locations = [val for val in self.next_locations if val not in self.past_locations]
                logging.info('Next locations: %s' % next_locations)
                self.next_locations = []
                for location in next_locations:
                    logging.info('Location: %s' % location)
                    self.scrape_location(self.keywords, location)
                    time.sleep(10 * self.base_sleep_time * np.random.rand())
                loc_depth += 1
        except KeyboardInterrupt:
            pass


if __name__ == '__main__':
    logging.basicConfig(level=eval('logging.%s' % sys.argv[1]))
    keywords_list = sys.argv[2].replace('_', ' ').split(',')
    locations = sys.argv[3].split(',')
    for keywords in keywords_list:
        keywords = keywords.lower()
        YS = YellScrape(
            keywords=keywords, locations=locations, max_loc_depth=int(sys.argv[4]),
            run_proxy=sys.argv[5], base_sleep_time=int(sys.argv[6])
        )
        try:
            try:
                YS.scrape_yell()
            except KeyboardInterrupt:
                print('Keyboard Exit')
        except IOError:
            Exception('Something Broke')


